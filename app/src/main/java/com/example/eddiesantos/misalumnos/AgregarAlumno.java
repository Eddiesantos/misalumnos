package com.example.eddiesantos.misalumnos;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;


public class AgregarAlumno extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long alumnoId;
    private EditText nombreEditText;
    private EditText apellidoEditText;
    private EditText grupoEditText;
    private EditText puntosEditText;
    private EditText riesgoEditText;

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_alumno);

        nombreEditText =(EditText) findViewById(R.id.nombre_edit_text);
        apellidoEditText =(EditText) findViewById(R.id.apellido_edit_text);
        grupoEditText =(EditText) findViewById(R.id.grupo_edit_text);
        puntosEditText =(EditText) findViewById(R.id.puntos_edit_text);
        riesgoEditText =(EditText) findViewById(R.id.riesgo_edit_text);

        alumnoId=getIntent().getLongExtra(DetalleAlumno.EXTRA_ALUMNO_ID,-1L);
        if (alumnoId != -1L){
            getSupportLoaderManager().initLoader(0,null,this);
        }




        findViewById(R.id.boton_agregar).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        String nombreAlumno = nombreEditText.getText().toString();
        String apellidoAlumno = apellidoEditText.getText().toString();
        String grupoAlumno = grupoEditText.getText().toString();
        String puntosAlumno = puntosEditText.getText().toString();
        String riesgoAlumno = riesgoEditText.getText().toString();

        // llamamos al metodo que crea el animal y le pasamos los parametros
        if (nombreAlumno.isEmpty()||apellidoAlumno.isEmpty()||grupoAlumno.isEmpty() ){
            Toast.makeText(this,"!Uno ó mas datos no han sido capturados!...",Toast.LENGTH_SHORT).show();
        }
        else {
            new CreateAlumnoTask(this, nombreAlumno, apellidoAlumno, grupoAlumno, puntosAlumno, riesgoAlumno, alumnoId).execute();
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new AlumnoslLoader(this,alumnoId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data!=null && data.moveToFirst()){
            int nombreIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_NOMBRE);
            String nombreAlumno=data.getString(nombreIndex);

            int apellidoIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_APELLIDOS);
            String apellidoAlumno=data.getString(apellidoIndex);

            int grupoIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_GRUPO);
            String grupoAlumno=data.getString(grupoIndex);

            int puntosIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_PUNTOS);
            String puntosAlumno=data.getString(puntosIndex);

            int riesgoIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_RIESGO);
            String riesgoAlumno=data.getString(riesgoIndex);

           nombreEditText.setText(nombreAlumno);
           apellidoEditText.setText(apellidoAlumno);
            grupoEditText.setText(grupoAlumno);
            puntosEditText.setText(puntosAlumno);
            riesgoEditText.setText(riesgoAlumno);


        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    public static class CreateAlumnoTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String alumnoNombre;
        private String alumnoApellidos;
        private String alumnoGrupo;
        private String alumnoPuntos;
        private String alumnoRiesgo;

        private long alumnoId;


        public CreateAlumnoTask(Activity activity, String nombre, String apellidos, String grupo, String puntos, String riesgo,  long alumnoId){
            weakActivity = new WeakReference<Activity>(activity);
            alumnoNombre = nombre;
            alumnoApellidos = apellidos;
            alumnoGrupo = grupo;
            alumnoPuntos = puntos;
            alumnoRiesgo= riesgo;
            this.alumnoId = alumnoId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();
            boolean succes = false;

            if (alumnoId!=-1L){
                int filasAfectadas=AlumnosDatabase.actualizaAlumno(appContext, alumnoNombre, alumnoApellidos, alumnoGrupo, alumnoPuntos, alumnoRiesgo,alumnoId);
                succes=(filasAfectadas != 0);
            } else {
                long id=AlumnosDatabase.insertAlumno(appContext, alumnoNombre, alumnoApellidos, alumnoGrupo, alumnoPuntos, alumnoRiesgo);
                succes=(id != -1L);
            }

            return  succes;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }

            context.finish();
        }
    }

}

