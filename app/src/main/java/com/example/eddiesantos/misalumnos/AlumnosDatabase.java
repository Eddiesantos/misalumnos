package com.example.eddiesantos.misalumnos;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by EddieSantos on 17/11/2016.
 */



public class AlumnosDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "alumnos.db";
    private static final String TABLE_NAME = "alumnos";
    public static final String COL_NOMBRE = "nombre";
    public static final String COL_APELLIDOS = "apellidos";
    public static final String COL_GRUPO = "grupo";
    public static final String COL_PUNTOS = "puntos";
    public static final String COL_RIESGO = "riesgo";

    public AlumnosDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery = "CREATE TABLE " + TABLE_NAME +
                "  (_id INTEGER PRIMARY KEY, "
                + COL_NOMBRE + " TEXT NOT NULL COLLATE UNICODE, "
                + COL_APELLIDOS + " TEXT NOT NULL, "
                + COL_GRUPO + " TEXT NOT NULL, "
                + COL_PUNTOS + " TEXT NOT NULL, "
                + COL_RIESGO + " TEXT NOT NULL)";

        db.execSQL(createQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String upgradeQuery = "DROP TABLE IF EXIST " + TABLE_NAME;
        db.execSQL(upgradeQuery);
    }

    public static long insertAlumno(Context context, String nombre, String apellidos, String grupo, String puntos, String riesgo) {
        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues valorAlumno = new ContentValues();
        valorAlumno.put(COL_NOMBRE, nombre);
        valorAlumno.put(COL_APELLIDOS, apellidos);
        valorAlumno.put(COL_GRUPO, grupo);
        valorAlumno.put(COL_PUNTOS, puntos);
        valorAlumno.put(COL_RIESGO, riesgo);

        long result = -1L;

        try {

            result = database.insert(TABLE_NAME, null, valorAlumno);
            if (result != -1L) {
                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(AlumnosLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

        } finally {
            dbOpenHelper.close();
        }


//        long result = database.insert(TABLE_NAME,null,valorAnimal);
//        dbOpenHelper.close();
        return result;


    }

    public static Cursor devuelveTodos(Context context) {
        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME, new String[]{COL_NOMBRE, COL_APELLIDOS, BaseColumns._ID}
                , null, null, null, null,
                COL_NOMBRE + " ASC");

    }

    public static Cursor devuelveConId(Context context, long identificador) {
        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();


        return database.query(
                TABLE_NAME, new String[]{COL_NOMBRE, COL_APELLIDOS, COL_GRUPO, COL_PUNTOS, COL_RIESGO, BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null,
                null,
                COL_APELLIDOS + " ASC");

    }

    public static int eliminaConId(Context context, long AlumnosId) {
        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME, BaseColumns._ID + " =?",
                new String[]{String.valueOf(AlumnosId)});

        if (resultado != 0) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(AlumnosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return resultado;
    }

    public static int actualizaAlumno(Context context, String nombre, String apellidos,
                                      String grupo, String puntos, String riesgo, long alumnoId) {
        SQLiteOpenHelper dbOpenHelper = new AlumnosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorAlumno = new ContentValues();
        valorAlumno.put(COL_NOMBRE, nombre);
        valorAlumno.put(COL_APELLIDOS, apellidos);
        valorAlumno.put(COL_GRUPO, grupo);
        valorAlumno.put(COL_PUNTOS, puntos);
        valorAlumno.put(COL_RIESGO, riesgo);


        int result = database.update(
                TABLE_NAME,
                valorAlumno, BaseColumns._ID + " =?",
                new String[]{String.valueOf(alumnoId)});

        if (result != 0) {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(AlumnosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return result;
    }
}

