package com.example.eddiesantos.misalumnos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DetalleAlumno extends AppCompatActivity  implements LoaderManager.LoaderCallbacks<Cursor> {
    // crear variables por conveniencia privadas
    private TextView nombreTextView;
    private TextView apellidoTextView;
    private TextView grupoTextView;
    private TextView puntosTextView;
    private TextView riesgoTextView;
    private long alumnoId;
    public static final String EXTRA_ALUMNO_ID="alumno.id.extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_alumno);

        nombreTextView = (TextView) findViewById(R.id.nombre_text_view);
        apellidoTextView=(TextView) findViewById(R.id.apellido_text_view);
        grupoTextView=(TextView) findViewById(R.id.grupo_text_view);
        puntosTextView=(TextView) findViewById(R.id.puntos_text_view);
        riesgoTextView=(TextView) findViewById(R.id.riesgo_text_view);

        Intent intencion = getIntent();

        alumnoId=intencion.getLongExtra(MainActivity.EXTRA_ID_ALUMNO,-1L);

        getSupportLoaderManager().initLoader(0,null,this);

        findViewById(R.id.boton_eliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DeleteAlumnoTask(DetalleAlumno.this,alumnoId).execute();
                    }
                }
        );

        findViewById(R.id.boton_actualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(DetalleAlumno.this,AgregarAlumno.class);
                        intent.putExtra(EXTRA_ALUMNO_ID,alumnoId);
                        startActivity(intent);
                    }
                }
        );

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new AlumnoslLoader(this,alumnoId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data!=null && data.moveToFirst()){
            int nombreIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_NOMBRE);
            String nombreAlumno=data.getString(nombreIndex);

            int apellidoIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_APELLIDOS);
            String apellidoAlumno=data.getString(apellidoIndex);

            int grupoIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_GRUPO);
            String grupoAlumno=data.getString(grupoIndex);

            int puntosIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_PUNTOS);
            String puntosAlumno=data.getString(puntosIndex);

            int riesgoIndex=data.getColumnIndexOrThrow(AlumnosDatabase.COL_RIESGO);
            String riesgoAlumno=data.getString(riesgoIndex);

            nombreTextView.setText("NOMBRE: "+nombreAlumno);
            apellidoTextView.setText("APELLIDOS: "+apellidoAlumno);
            grupoTextView.setText("GRUPO: "+grupoAlumno);
            puntosTextView.setText("PUNTOS: "+puntosAlumno);
           riesgoTextView.setText("RIESGO: "+riesgoAlumno);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class DeleteAlumnoTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private Long alumnoId;

        public DeleteAlumnoTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
           alumnoId=id;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            int filasAfectadas = AlumnosDatabase.eliminaConId(appContext, alumnoId);
            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }

            context.finish();
        }
    }

}