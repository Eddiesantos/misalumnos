package com.example.eddiesantos.misalumnos;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by EddieSantos on 17/11/2016.
 */

public class MiAplicacion extends Application {
    public void onCreate(){
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
